import React from 'react';
import Slider from "react-slick";
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { breakpoints, colors, fonts } from '../styles/variables';

import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';

const propTypes = {
  SliderTestimonials: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string
  })).isRequired
};

const StyledImageSlider = styled(Slider)`
  position: relative;
  overflow: hidden;
  padding: 9.5rem 2rem 5rem 2rem;
  z-index: 1;

  @media ${breakpoints.medium} {
      padding: 15rem 0 0 0;
    }

  :before,:after {
    content: '';
    position: absolute;
    right: -25%;
    top: 0;
    z-index: -1;
    width: 150%;
    height: 100%;
    background: ${colors.eclipDarkGreen};
    transform-origin: 0 0;
    box-sizing: border-box;

    @media ${breakpoints.medium} {
      transform: rotate(2deg);
    }
  }

  :after {
    height: 100%;
    width: 150%;
    left: -25%;
    background: ${colors.eclipGreen};
    transform: rotate(3deg);
    transform-origin: 3% 0;
    z-index: -1;
    box-sizing: border-box;

    @media ${breakpoints.medium} {
      transform: rotate(3deg);
    }
  }
`;

const Testimonial = styled.div`
  background: ${colors.eclipGreen};
  min-height: 20rem;
  margin: 0 auto;
  position: relative;
  text-align: center;
  outline: none;
`;

const Name = styled.p`
  font-size: 2rem;
  text-align: right;
  padding-top: 3rem;
  font-family: ${fonts.roboto};
  color: ${colors.snowWhite};

  @media ${breakpoints.medium} {
    padding: 0;
    text-align: left;
    position: absolute;
    bottom: 25%;
    right: 80%;
    transform: translate3d(80%,-25%,0);
  }
`;

const Description = styled.p`
  font-size: 3rem;
  color: ${colors.snowWhite};
  font-family: ${fonts.robotoBold};
  margin: 0 auto;
`;

const settings = {
  dots: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 6000,
  speed: 800,
  slidesToShow: 1,
  adaptiveHeight: true,
  arrows: false
};

const Testimonials = ({ SliderTestimonials }) => {
  return (
    <StyledImageSlider {...settings}>
      {
        SliderTestimonials.map(({ name, description }, key) => (
          <Testimonial key={key}>
            <Description>{description}</Description>
            <Name>- {name}</Name>
          </Testimonial>)
        )}
    </StyledImageSlider>
  );
};

Testimonials.propTypes = propTypes;
export default Testimonials;
