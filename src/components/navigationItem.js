import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { colors, fonts, breakpoints } from '../styles/variables';


const propTypes = {
  item: PropTypes.shape({
    url: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
  }).isRequired,
  isTop: PropTypes.bool.isRequired
};

const NavigationItemWrapper = styled.li`
  display: block;
  padding: 2rem .5rem;
  cursor: pointer;
  @media ${breakpoints.medium} {
    padding: 1rem;
  }
`;

const StyledAnchorLink = styled(AnchorLink)`
  color: ${colors.black};
  transition: all .5s ease-in-out;
  text-decoration: none;
  font-size: 3rem;
  font-family: ${fonts.robotoBold};

  &:hover {
    color: ${colors.eclipGreen};
  }

  @media ${breakpoints.medium} {
    font-size: 2rem;
    color: ${({ isTop }) => isTop ? `${colors.snowWhite}` : `${colors.black}` };
    text-shadow: ${({ isTop }) => isTop ? `${colors.black} 1px 1px 1px;` : '' };
  }
`;

const StyledLink = styled(Link)`
  color: ${colors.black};
  transition: all .5s ease-in-out;
  text-decoration: none;
  font-size: 3rem;
  font-family: ${fonts.robotoBold};

  &:hover {
    color: ${colors.eclipGreen};
  }

  @media ${breakpoints.medium} {
    font-size: 2rem;
    color: ${({ isTop }) => isTop ? `${colors.snowWhite}` : `${colors.black}` };
    text-shadow: ${({ isTop }) => isTop ? `${colors.black} 1px 1px 1px;` : '' };
  }
`;

const NavigationItem = ({ item, isTop }) => (
  <NavigationItemWrapper>
    {
      item.type === 'link' ? <StyledLink to={item.url} isTop={isTop}>{item.name}</StyledLink> :
        <StyledAnchorLink href={item.url} isTop={isTop}>{item.name}</StyledAnchorLink>
    }
  </NavigationItemWrapper>
);

NavigationItem.propTypes = propTypes;
export default NavigationItem;
