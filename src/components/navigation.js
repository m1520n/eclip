import React from 'react';
import { Link } from 'react-router-dom';

import styled from 'styled-components';
import PropTypes from 'prop-types';

import { colors, breakpoints } from '../styles/variables';
import NavigationItem from './navigationItem';

const propTypes = {
  expanded: PropTypes.bool.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({
    url: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
  onClickHandler: PropTypes.func.isRequired,
  isTop: PropTypes.bool.isRequired
};

const defaultProps = {
  isTop: false
};

const NavWrapper = styled.nav`
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 1000;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  background-color: ${colors.snowWhite};
  padding: 1rem;
  transition: all .5s ease-in-out;
  box-sizing: border-box;

  @media ${breakpoints.medium} {
    padding: ${({ isTop }) => isTop ? '2rem' : '.3rem' };
    background-color: ${({ isTop }) => isTop ? 'transparent' : `${colors.snowWhite}` };
    border-bottom: ${({ isTop }) => isTop ? '' : `.3rem solid ${colors.eclipDarkGreen}` };

  }
`;

const MenuItems = styled.ul`
  text-align: center;
  display: ${({ expanded }) => expanded ? 'flex' : 'none'};
  flex-direction: column;
  text-transform: uppercase;
  margin: 0;
  width: 100%;

  @media ${breakpoints.medium} {
    display: flex;
    flex-direction: row;
    padding: 0 .5rem;
    width: initial;
  }
`;

const Bar = styled.span`
  display: inline-block;
  position: absolute;
  width: 100%;
  height: .5rem;
  background: ${colors.black};
  border-radius: .5rem;
  opacity: 1;
  right: 0;
  transform: rotate(0deg);
  transition: .25s ease-in-out;
`;

const Logo = styled.div`
  margin-left: 1rem;
  height: 5rem;
  width: 11.39rem;
  background-image: url('/images/logo.png');
  background-repeat: no-repeat;
  background-size: cover;
`;

const Hamburger = styled.div`
  margin-right: 1.5rem;
  width: 4rem;
  height: 3rem;
  visibility: visible;
  position: relative;
  transform: rotate(0deg);
  transition: .5s ease-in-out;
  cursor: pointer;

  span:nth-child(1) {
    top: 0;
    transform-origin: left center;
    ${({ expanded }) => expanded ? 'transform: rotate(45deg); top: -.3rem;' : ''};
  }

  span:nth-child(2) {
    top: 1.25rem;
    transform-origin: left center;
    ${({ expanded }) => expanded ? 'width: 0%; opacity: 0' : ''};
  }

  span:nth-child(3) {
    top: 2.5rem;
    transform-origin: left center;
    ${({ expanded }) => expanded ? 'transform: rotate(-45deg);' : ''};
  }

  @media ${breakpoints.medium} {
    visibility: hidden;
    display: none;
  }
`;


const Navigation = ({ items, expanded, onClickHandler, isTop }) => {
  return (
    <NavWrapper expanded={expanded} isTop={isTop}>
      <Link to="/" onClick={() => window.scrollTo(0, 0)}>
        <Logo/>
      </Link>
      <Hamburger
        onClick={onClickHandler}
        expanded={expanded}
        for="MenuToggle"
      >
        <Bar/>
        <Bar/>
        <Bar/>
      </Hamburger>
      <MenuItems expanded={expanded}>
        { items.map((item, index) => (<NavigationItem item={item} isTop={isTop} expanded={expanded} key={index}/>)) }
      </MenuItems>
    </NavWrapper>
  );
};

Navigation.defaultProps = defaultProps;
Navigation.propTypes = propTypes;

export default Navigation;
