import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import { colors, breakpoints, fonts } from '../styles/variables';

import Icon from './common/icon';

const propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({
    heading: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    iconColor: PropTypes.string.isRequired
  })).isRequired,
  videoExpanded: PropTypes.bool
};

const ColumnImagesWrapper = styled.section`
  display: flex;
  flex-direction: column;
  padding-top: 9.5rem;
  overflow-x: hidden;
  position: relative;
  z-index: 1;

  @media ${breakpoints.medium} {
      padding-top: 14rem;
      flex-direction: row;
    }
  :before,:after {
    content: '';
    position: absolute;
    left: -25%;
    z-index: -1;
    width: 150%;
    height: 75%;
    background: ${colors.eclipGreen};
    transform: rotate(-3deg);
    transform-origin: 0 0;
    box-sizing: border-box;

    @media ${breakpoints.medium} {
      transform: rotate(-2deg);
    }
  }

  :before {
    height: 50%;
    background: ${colors.eclipDarkGreen};
    transform: rotate(-5.5deg);
    transform-origin: 0 0;
    z-index: -1;
    box-sizing: border-box;

    @media ${breakpoints.medium} {
      transform: rotate(-3deg);
    }
  }
`;

const Column = styled.div`
  display: flex;
  position: relative;
  background-color: ${colors.eclipGreen};
  color: ${colors.white};
  flex-direction: column;
  justify-content: space-between;
  flex-grow: 1;
  @media ${breakpoints.medium} {
    overflow: ${({ videoExpanded }) => videoExpanded ? 'visible' : 'hidden'};
    width: 33.333%;
  }
`;

const ColumnHeader = styled.div`
  display: flex;
  flex-direction: column;
  padding: 5rem 2rem;
  height: 30rem;
  position: relative;
  align-items: center;
  justify-content: space-between;
`;

const Heading = styled.h3`
  font-family: 'robotoCondensedBold';
  font-size: 3.5rem;
  text-transform: uppercase;
`;

const Description = styled.p`
  font-size: 2.5rem;
  color: ${colors.lightGrey};
`;

const Button = styled(Link)`
  font-size: 2.5rem;
  text-decoration: none;
  color: ${colors.snowWhite};
  padding: 2rem 5rem;
  margin: 2rem 0;
  font-family: ${fonts.robotoBold};
  background-color: ${colors.white};
  text-transform: uppercase;
  font-size: 2rem;
  color: ${colors.black};
  cursor: pointer;
  border: 0;
  border-radius: 1rem 0 1rem 0;
  transition: .5s;

  :hover {
    border-radius: 0 1rem 0 1rem;
    box-shadow: .5rem .5rem 0 0 ${colors.eclipDarkGreen};
  }

  @media ${breakpoints.medium} {
    padding: 1rem 4rem;
    font-size: 1.5rem;
  }
`;

const Image = styled.img`
  width: 100%;
  height: 45rem;
  object-fit: cover;
`;

const ColumnImages = ({ columns }) => (
  <ColumnImagesWrapper>
    {
      columns.map(({ heading, description, image, icon, iconColor, route }, index) => (
        <Column key={index}>
          <ColumnHeader>
            <Heading>{heading}</Heading>
            <Description>{description}</Description>
            <Icon size="large" icon={icon} color={iconColor} />
            <Button onClick={() => scrollTo(0, 0)} to={route}>Zobacz</Button>
          </ColumnHeader>
          <Image src={image} />
        </Column>
      ))
    }
  </ColumnImagesWrapper>
);

ColumnImages.propTypes = propTypes;
export default ColumnImages;
