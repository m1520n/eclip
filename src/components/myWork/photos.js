import React from 'react';

import SubPage from './subPage';

import PropTypes from 'prop-types';

const propTypes = {
  title: PropTypes.string.isRequired,
  header: PropTypes.string.isRequired,
  sliderImages: PropTypes.arrayOf(PropTypes.shape({
    url: PropTypes.string
  })).isRequired
};

const Photos = ({ sliderImages, header, title }) => (
  <SubPage
    title={title}
    header={header}
    sliderImages={sliderImages}
  >
  Dzięki możliwości zamocowania kamery na dronie, jesteśmy w stanie umieścić ją
  w dowolnym punkcie w przestrzeni, co sprawia że możemy przedstawić każde wydarzenie lub obiekt z każdej strony.
  Planujesz dużą uroczystość? Jeśli wzbogacisz film o kilka ujęć z
  powietrza z całą pewnością film będzie atrakcyjniejszy. Masz nieruchomość którą chcesz sprzedać?
  Możesz pokazać same zdjęcia nieruchomości,ale również wspaniałą okolicę, wyostrzając
  tym samym atrakcyjność Twojej oferty.
  </SubPage>
);

Photos.propTypes = propTypes;
export default Photos;
