import React from 'react';

import SubPage from './subPage';

import PropTypes from 'prop-types';

const propTypes = {
  title: PropTypes.string.isRequired,
  header: PropTypes.string.isRequired,
  sliderImages: PropTypes.arrayOf(PropTypes.shape({
    url: PropTypes.string
  })).isRequired
};

const Clips = ({ sliderImages, header, title }) => (
  <SubPage
    title={title}
    header={header}
    sliderImages={sliderImages}
  >
  Wydawałoby się, że jednominutowy film, to jedna minuta z czyjegoś życia,
  z jakiegoś wydarzenia. Nic bardziej mylnego. Bardzo często jednominutowy film to godziny materiału.
  Selekcja odpowiednich fragmentów to kluczowa sprawa, bardzo często wydaje nam się, że mamy świetne ujęcie,
  a jednak okazuje się że ktoś akurat kichnął, ktoś przeszedł przez kadr.
  Bardzo często jednominutowy film to dziesiątki albo nawet setki GB materiału, który trzeba przeglądać godzinami,
  wybrać odpowiednie fragmenty, skleić w całość,
  dopasować do odpowiedniej muzyki którą wcześniej również trzeba znaleść,
  wyostrzyć najbardziej interesującą nas chwilę filmu i dobrze zakończyć żeby film był przyjemny i ciekawy.
  </SubPage>
);

Clips.propTypes = propTypes;
export default Clips;
