import React from 'react';

import SubPage from './subPage';

import PropTypes from 'prop-types';

import styled from 'styled-components';

const propTypes = {
  title: PropTypes.string.isRequired,
  header: PropTypes.string.isRequired,
  sliderImages: PropTypes.arrayOf(PropTypes.shape({
    url: PropTypes.string
  })).isRequired
};

const List = styled.ul`
  padding: 2rem;
  list-style-type: disc;
`;

const ListItem = styled.li`
  font-size: 3rem;
  margin-left: 1rem;
`;


const Advertisement = ({ sliderImages, header, title }) => (
  <SubPage
    title={title}
    header={header}
    sliderImages={sliderImages}
  >
  Reklama dźwignią handlu - głosi stare przysłowie.
  Ile razy dostajemy ulotkę i na nią nie spoglądamy?
  Wiele razy. Jeśli jednak ulotka, plakat czy reklama w jakiejkolwiek formie jest ciekawa?
  Opowiadamy o niej znajomym i przyjaciołom.
  Dlatego właśnie zajmuję się projektowaniem i kompleksowym wykonaniem reklam tradycyjnych w kreatywny sposób.
  Jeśli na ulicy zobaczysz kaseton LED który nie szpeci okolicy ale wprost przeciwnie zachęca do wizyty to
  jest szansa że to mój projekt. Jeśli dostaniesz niebanalną ulotkę, lub ktoś podaruje Ci wizytówkę która zwróci
  na siebie uwagę w gąszczu innych to jest szansa że to również mój projekt.
    <hr/>
  Wykonuję :
    <List>
      <ListItem>Kasetony/Szyldy LED</ListItem>
      <ListItem>Ulotki</ListItem>
      <ListItem>Wizytówki</ListItem>
      <ListItem>Roll-up</ListItem>
    </List>
  ale, prowadzę również z kilkoma moimi klientami szeroko zakrojone kampanie reklamowe,
  które w bardzo dużym stopniu spotykają się z dużą aprobatą klientów moich klientów.
  </SubPage>
);

Advertisement.propTypes = propTypes;
export default Advertisement;
