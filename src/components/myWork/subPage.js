import React from 'react';
import Slider from "react-slick";
import { Helmet } from 'react-helmet';

import styled from 'styled-components';
import PropTypes from 'prop-types';


import { colors, fonts, breakpoints } from '../../styles/variables';

import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';

const propTypes = {
  title: PropTypes.string.isRequired,
  header: PropTypes.string.isRequired,
  sliderImages: PropTypes.arrayOf(PropTypes.shape({
    url: PropTypes.string
  })).isRequired
};

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  autoplay: true,
  appendDots: dots => (
    <div
      style={{
        position: 'absolute',
        bottom: '2rem',
        color: `${colors.snowWhite}`
      }}
    >
      <ul style={{ margin: "0" }}> {dots} </ul>
    </div>
  )
};


const WrapperWork = styled.section`
  display: flex;
  flex-direction: column;
  background-color: ${colors.snowWhite};
`;

const StyledImageSlider = styled(Slider)`
  width: 100%;
`;

const BgImage = styled.div`
  background-image: url('${({ image }) => image.url }');
  background-size: cover;
  height: 50rem;
  outline: none;
`;

const Container = styled.div`
  @media ${breakpoints.medium} {
    padding: 5rem;
  }
`;

const Header = styled.div`
  font-size: 5rem;
  text-align: center;
  padding-top: 5rem;
  font-family: ${fonts.robotoBold};
  margin-top: 20rem;
  color: ${colors.snowWhite};
  text-shadow: ${colors.black} 1px 1px 1px;

  @media ${breakpoints.medium} {
    font-size: 8rem;
  }
`;

const Text = styled.div`
  font-size: 3rem;
  line-height: 3rem;
  text-align: justify;
  padding: 2rem;
  margin: 2rem 0;
  background-color: ${colors.white};

  @media ${breakpoints.medium} {
    padding: 5rem;
    width: 60%;
    margin: 0 auto;
  }
`;

const SubPage = ({ title, sliderImages, children, header }) => (
  <WrapperWork>
    <Helmet>
      <title>{title}</title>
    </Helmet>
    <StyledImageSlider {...settings}>
      { sliderImages.map((image, key) => (
        <BgImage key={key} image={image}>
          <Header>
            {header}
          </Header>
        </BgImage>
      )
      ) }
    </StyledImageSlider>
    <Container>
      <Text>
        {children}
      </Text>
    </Container>
  </WrapperWork>
);

SubPage.propTypes = propTypes;
export default SubPage;
