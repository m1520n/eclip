import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { colors, breakpoints } from '../styles/variables';

const propTypes = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  agreement: PropTypes.bool.isRequired,
  messageSent: PropTypes.bool.isRequired,
  isValidated: PropTypes.bool.isRequired,
  setContactFormRef: PropTypes.func.isRequired,
  onTextInput: PropTypes.func.isRequired,
  onMessageSend: PropTypes.func.isRequired
};

const AGREEMENT = `
  Wyrażam zgodę na przetwarzanie danych osobowych zgodnie z ustawą o ochronie danych
  osobowych w związku z (np. wysłaniem zapytania przez formularz kontaktowy).
  Podanie danych jest dobrowolne, ale niezbędne do przetworzenia zapytania.
  Zostałem poinformowany, że przysługuje mi prawo dostępu do swoich danych,
  możliwosci ich poprawiania, żądania zaprzestania ich przetwarzania.
  Administratorem danych osobowych jest eClip, ul.Joliot-Curie 28/1, 41-813 Zabrze.
`;

const ContactWrapper = styled.section`
  display: flex;
  justify-content: space-around;
  align-items: center;
  flex-direction: column;
  background-color: ${colors.eclipGreen};
  padding: 5rem 2rem;
  position: relative;
  @media ${breakpoints.medium} {
    flex-direction: row;
  }
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;

  @media ${breakpoints.medium} {
    max-width: 50rem;
  }
`;

const TextInput = styled.input`
  margin: 1rem 0;
  border: .1rem solid ${colors.snowWhite};
  color: ${colors.snowWhite};
  background-color: ${colors.eclipGreen};
  height: 4rem;
  outline: none;
  font-size: 1.5rem;
  padding: 1rem 2rem;

  ::placeholder {
    color: ${colors.white};
  }
`;

const TextArea = styled.textarea`
  margin: 1rem 0;
  border: .1rem solid ${colors.snowWhite};
  color: ${colors.snowWhite};
  background-color: ${colors.eclipGreen};
  outline: none;
  font-size: 1.5rem;
  padding: 2rem;
  height: 20rem;

  ::placeholder {
    color: ${colors.white};
  }
`;

const Button = styled.button`
  padding: 2rem 4rem;
  margin: 2rem 0;
  background-color: ${colors.white};
  text-transform: uppercase;
  font-size: 1.5rem;
  color: ${colors.black};
  cursor: pointer;
  border: 0;
  border-radius: 1rem 0 1rem 0;
  transition: .5s;
  height: 6rem;

  :hover {
    border-radius: 0 1rem 0 1rem;
    box-shadow: .5rem .5rem 0 0 ${colors.eclipDarkGreen};
  }
`;

const ThankYou = styled.p`
  font-size: 3rem;
  color: ${colors.snowWhite};
`;

const ContactInfo = styled.div`
  padding: 3rem;
  display: flex;
  flex-direction: column;
`;

const Text = styled.p`
  font-size: 2.5rem;
  color: ${colors.white};
  padding-bottom: 2rem;
  word-wrap: break-word;
`;

const Phone = styled.a`
  font-size: 4rem;
  color: ${colors.white};
  padding-bottom: 2rem;
  text-decoration: none;
  white-space: nowrap;
`;

const Checkbox = styled.input`
  margin: 0;
  margin-right: 1rem;
`;

const Agreement = styled.label`
  display: flex;
  color: ${colors.white};
  text-align: justify;
  margin-top: 1rem;
`;

const Error = styled.p`
  font-size: 1.5rem;
  color: ${colors.white};
`;

const Contact = ({
  name,
  email,
  message,
  messageSent,
  setContactFormRef,
  onTextInput,
  onMessageSend
}) => (
  <ContactWrapper>
    {
      messageSent ? <ThankYou>Dziękuję za wiadomość</ThankYou> :
        <Form innerRef={(el) => setContactFormRef(el)}>
          <TextInput
            type="text"
            name="name"
            placeholder="Firma / Imię Nazwisko"
            value={name}
            onChange={onTextInput}
            required
            title="To pole jest wymagane"
          />
          <Error />

          <TextInput
            type="email"
            name="email"
            placeholder="e-mail"
            value={email}
            onChange={onTextInput}
            required
          />
          <Error />

          <TextArea
            type="textarea"
            name="message"
            placeholder="Wiadomość"
            value={message}
            onChange={onTextInput}
            required
          />
          <Error />

          <Agreement>
            <Checkbox type="checkbox" required />
            {AGREEMENT}
          </Agreement>
          <Error />

          <Button onClick={onMessageSend}>Wyślij!</Button>
        </Form>
    }
    <ContactInfo>
      <Text>ul.Joliot-Curie 28/1 * 41-813 Zabrze</Text>
      <Text>krzysiek@eclip.pl</Text>
      <Phone href="tel:+48695868976">+48 695 868 976</Phone>
    </ContactInfo>
  </ContactWrapper>
);

Contact.propTypes = propTypes;

export default Contact;
