import React from 'react';
import Lightbox from 'react-images';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { breakpoints, colors } from '../styles/variables';
import Icon from './common/icon';

const propTypes = {
  images: PropTypes.arrayOf(PropTypes.shape({
    url: PropTypes.string
  })).isRequired
};

const GalleryWrapper = styled.section`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  & div:nth-child(n+4) {
    display: none;
  }

  @media ${breakpoints.medium} {
    & div:nth-child(n+4) {
      display: block;
    }
  }
`;

const Image = styled.div`
  flex: 100%;
  position: relative;
  height: 40rem;
  max-width: 100%;
  cursor: pointer;

  :hover :nth-child(2) {
    opacity: .5;
  }

  :hover :nth-child(3) {
    opacity: 1;
  }

  @media ${breakpoints.medium} {
    flex: 50%;
    max-width: 50%;
  }

  @media ${breakpoints.large} {
    flex: 33.333%;
    max-width: 33.333%;
  }
`;

const Thumbnail = styled.div`
  width: 100%;
  height: 100%;
  background-image: url('${({ url }) => url}');
  background-repeat: no-repeat;
  background-size: cover;
`;

const ColorOverlay = styled.div`
  position: absolute;
  opacity: 0;
  top: 0;
  left: 0;
  background-color: ${colors.eclipGreen};
  width: 100%;
  height: 100%;
  transition: all .2s ease-in-out;
`;

const StyledIcon = styled.div`
  position: absolute;
  display: flex;
  width: 100%;
  height: 100%;
  top: 0;
  justify-content: center;
  align-items: center;
  opacity: 0;
  transition: opacity .2s ease-in-out;
`;

class Gallery extends React.Component {
  constructor () {
    super();

    this.state = {
      isOpen: false,
      currentImage: 0
    };
  }

  openLightbox = (i, e) => {
    e.preventDefault();

    this.setState({
      currentImage: i,
      isOpen: true
    });
  }

  onClickPrev = () => {
    this.setState({
      currentImage: this.state.currentImage - 1
    });
  }

  onClickNext = () => {
    this.setState({
      currentImage: this.state.currentImage + 1
    });
  }

  onClose = () => {
    this.setState({
      isOpen: false
    });
  }

  renderGallery () {
    const { images } = this.props;

    if (!images) return;

    return images
      .map((image, i) => (
        <Image key={i} onClick={(e) => this.openLightbox(i, e)}>
          <Thumbnail
            url={image.src}
          />
          <ColorOverlay/>
          <StyledIcon><Icon size="medium" icon="search" color="white" /></StyledIcon>
        </Image>
      ));
  }

  render () {
    const { images } = this.props;
    const { isOpen, currentImage } = this.state;

    return (
      <GalleryWrapper>
        { this.renderGallery() }
        <Lightbox
          images={images}
          isOpen={isOpen}
          closeButtonTitle="Zamknij"
          currentImage={currentImage}
          onClickPrev={this.onClickPrev}
          onClickNext={this.onClickNext}
          onClose={this.onClose}
        />
      </GalleryWrapper>
    );
  }
}

Gallery.propTypes = propTypes;

export default Gallery;
