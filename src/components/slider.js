import React from 'react';
import styled, { keyframes } from 'styled-components';
import PropTypes from 'prop-types';

import { colors, breakpoints } from '../styles/variables';

import Icon from './common/icon';

// import ReactLoading from 'react-loading';

const propTypes = {
  text: PropTypes.string.isRequired,
  sliderActive: PropTypes.number.isRequired,
  onLoaded: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired
};

const ParallaxImage = styled.section`
  display: flex;
  position: relative;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  height: 50rem;
  @media ${breakpoints.medium} {
    height: 100vh;
  }
`;

const Overlay = styled.div`
  opacity: ${({ isLoading }) => isLoading ? 0 : '.5' };
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left:0;
  background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAFUlEQVQYV2NkQAOM+AT+gySRVYAFABvTAgMQ3puPAAAAAElFTkSuQmCC') repeat;
  z-index: 10;
`;

const Rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const SpinnerIcon = styled.span`
  animation: ${Rotate360} 2s linear infinite;
  display: ${({ isLoading }) => isLoading ? 'block' : 'none' };
`;

const MainText = styled.h1`
  font-family: 'robotoCondensedBold';
  font-size: 5rem;
  color: ${colors.white};
  text-transform: uppercase;
  opacity: ${({ visible }) => visible};
  transform: translate(${({ visible }) => visible ? 0 : '-10rem'}, 0);
  transition: all .5s ease-in;
  text-shadow: .3rem .3rem ${colors.eclipDarkGreenTransparent};
  z-index: 100;
  padding: 2rem;

  @media ${breakpoints.medium} {
    font-size: 7rem;
  }
`;

const Video = styled.video`
  position: absolute;
  right: 0;
  bottom: 0;
  min-width: 100%;
  min-height: 100%;
  width: auto;
  height: auto;
  background-size: cover;
  overflow: hidden;
`;

const Slider = ({ text, sliderActive, onLoaded, isLoading }) => {
  return (
    <ParallaxImage>
      <SpinnerIcon isLoading={isLoading}><Icon size="large" icon="spinner" color={colors.eclipGreen} /></SpinnerIcon>
      <MainText visible={sliderActive}>{text}</MainText>
      <Overlay isLoading={isLoading}/>
      <Video
        onLoadedData={onLoaded}
        playsinline
        loop
        width="100%"
        autoPlay
        muted
        src="/videos/1080p.mp4"
      />
    </ParallaxImage>
  );
};

Slider.propTypes = propTypes;
export default Slider;
