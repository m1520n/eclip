import React from 'react';
import styled from 'styled-components';
import { SocialIcon } from 'react-social-icons';

import { colors, breakpoints, fonts } from '../styles/variables';


const FooterWrapper = styled.footer`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  background-color: ${colors.eclipDarkGreen};
  height: 14rem;
  flex-direction: column;
  padding: 2rem;

  @media ${breakpoints.medium} {
    flex-direction: row;
  }
`;

const SocialIcons = styled.span`
  display: flex;
`;

const Copyright = styled.span`
  color: ${colors.eclipGreen};
  font-size: 2rem;
  font-family: ${fonts.roboto};
`;

const MadeByLink = styled.a`
  color: ${colors.eclipGreen};
  text-decoration: none;
  font-size: 2rem;
  font-family: ${fonts.roboto};

  :hover {
    text-decoration: underline;
  }
`;

const Footer = () => (
  <FooterWrapper>
    <SocialIcons>
		  <SocialIcon color="#fff" url="https://www.facebook.com/eclipPL" network="facebook" />
	  </SocialIcons>
    <MadeByLink href="http://devbrains.pl">by DevBrains</MadeByLink>
    <Copyright>{`Copyright eClip ${new Date().getFullYear()}`}</Copyright>
  </FooterWrapper>
);

export default Footer;
