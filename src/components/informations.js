import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { colors, breakpoints } from '../styles/variables';

const propTypes = {
  heading: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  about: PropTypes.string.isRequired
};

const InfoWrapper = styled.section`
  color: ${colors.snowWhite};
  position: relative;
  overflow: hidden;
  padding: 2rem 2rem 5rem 2rem;
  z-index: 1;
  background: ${colors.eclipGreen};

`;

const MainText = styled.h2`
  font-family: 'robotoCondensedBold';
  font-size: 3.5rem;
  padding: 0 0 1rem 0;
  white-space: normal;

  @media ${breakpoints.medium} {
    font-size: 6rem;
    margin: 0 auto;
    width: 50%;
  }

  @media ${breakpoints.large} {
    width: 40%;
  }
`;

const Description = styled.p`
  font-size: 2rem;
  padding: 2rem 0;

  @media ${breakpoints.medium} {
    padding: 2rem 0 4rem 0;
  }
`;

const About = styled.p`
  font-size: 1.8rem;
  padding: 1rem 0;
  margin: 0 auto;
  line-height: 1.6em;
  text-align: justify;

  @media ${breakpoints.medium} {
    width: 50%;
  }

  @media ${breakpoints.large} {
    width: 40%;
  }
`;

const Informations = ({ heading, description, about }) => (
  <InfoWrapper>
    <MainText>{heading}</MainText>
    <Description>{description}</Description>
    <About>{about}</About>
  </InfoWrapper>
);

Informations.propTypes = propTypes;
export default Informations;
