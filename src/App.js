/* globals window */

import React, { Fragment } from 'react';
import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom';

import CookieBanner from 'react-cookie-banner';

import Navigation from './components/navigation';
import Advertisement from './components/myWork/advertisement';
import Clips from './components/myWork/clips';
import Photos from './components/myWork/photos';
import Home from './Home';
import Footer from './components/footer';

import { debounce } from './utils';

const mainNavigation = [
  {
    url: '#offer',
    name: 'oferta',
    type: 'anchor'
  },
  {
    url: '#gallery',
    name: 'galeria',
    type: 'anchor'
  },
  {
    url: '#contact',
    name: 'kontakt',
    type: 'anchor'
  }
];

const subNavigation = [
  {
    url: '/',
    name: 'Główna',
    type: 'link'
  }
];

const advertisementTitle = 'eClip.pl - Odmienny punkt widzenia - Reklama';
const clipsTitle = 'eClip.pl - Odmienny punkt widzenia - Filmy';
const photosTitle = 'eClip.pl - Odmienny punkt widzenia - Zdjecia';

const advertisementMain = "REKLAMA";
const clipsMain = "EDYCJA WIDEO";
const photosMain = "FOTOGRAFIA Z POWIETRZA";

const sliderImages = [
  {
    url: `https://images.unsplash.com/photo-1518995372969-424a232ca9ce?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=c5c6d
    ff34b99791fad74fc5a52ebfd00&auto=format&fit=crop&w=800&q=70`
  },
  {
    url: 'https://picsum.photos/600/400/?random'
  },
  {
    url: `https://images.unsplash.com/photo-1516131206008-dd041a9764fd?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5cdc
    76432883b62db78cb4b3f10e5d66&auto=format&fit=crop&w=800&q=70`
  }
];

const cookieBannerstyles = {
  banner: {
    position: 'fixed',
    bottom: 0,
    fontFamily: 'Source Sans Pro',
    height: 'auto',
    background: 'rgba(52, 64, 81, 0.88) url(/cookie.png) 20px 50% no-repeat',
    fontSize: '1.8rem',
    fontWeight: 600
  },
  button: {
    border: '1px solid white',
    height: '3.2rem',
    lineHeight: '3.2rem',
    background: 'transparent',
    color: 'white',
    fontSize: '1.4rem',
    fontWeight: 600,
    right: 20
  },
  message: {
    display: 'block',
    padding: '2rem 11rem 2rem 2rem',
    textAlign: 'left',
    color: 'white',
    lineHeight: '1rem'
  },
  link: {
    textDecoration: 'none',
    fontWeight: 'bold'
  }
};

class App extends React.Component {
  constructor () {
    super();

    this.state = {
      menuExpanded: false,
      isTop: true
    };

    this.debouncedOnScroll = debounce(this.onScroll, 50);
  }

  componentDidMount () {
    window.addEventListener('scroll', this.debouncedOnScroll);
  }

  componentWillUnmount () {
    window.remove.eventListener('scroll', this.debouncedOnScroll);
  }

  onToggleMenu = () => {
    this.setState({
      menuExpanded: !this.state.menuExpanded
    });
  }

  onScroll = () => {
    this.setState({
      isTop: (window.scrollY < 50)
    });
  }
  onScrollTop = () => {
    window.scrollTo(0, 0);
  }

  render () {
    const {
      isTop,
      menuExpanded
    } = this.state;

    return (
      <Fragment>
        <div id="top"/>
        <BrowserRouter>
          <Route render={({ location }) => (
            <Fragment>
              <Navigation
                items={location.pathname === "/" ? mainNavigation : subNavigation}
                expanded={menuExpanded}
                onClickHandler={this.onToggleMenu}
                isTop={isTop}
                onScroll={this.onScroll}
              />
              <Switch location={location}>
                <Route exact path="/" component={Home} />

                <Route path="/reklama" render={(props) => (<Advertisement {...props}
                  title={advertisementTitle}
                  header={advertisementMain}
                  sliderImages={sliderImages}
                />)} />

                <Route path="/filmy" render={(props) => (<Clips {...props}
                  title={clipsTitle}
                  header={clipsMain}
                  sliderImages={sliderImages}
                />)} />

                <Route path="/zdjecia" render={(props) => (<Photos {...props}
                  title={photosTitle}
                  header={photosMain}
                  sliderImages={sliderImages}
                />)} />
                <Route component={Home}/>
              </Switch>
            </Fragment>
          )}/>
        </BrowserRouter>
        <CookieBanner
          styles={cookieBannerstyles}
          buttonMessage="Rozumiem"
          message="W ramach naszej witryny stosujemy pliki cookies w celu świadczenia
            Państwu usług na najwyższym poziomie,
            w tym w sposób dostosowany do indywidualnych potrzeb. Korzystanie z witryny bez zmiany ustawień dotyczących
            cookies oznacza, że będą one zamieszczane w Państwa urządzeniu końcowym." />
        <Footer/>
      </Fragment>
    );
  }
}

export default App;
