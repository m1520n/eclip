export const colors = {
  white: '#fff',
  black: '#000',
  snowWhite: '#fffafa',
  lightBlack: '#021414',
  eclipGreen: '#26C281',
  eclipDarkGreen: '#198256',
  eclipDarkGreenTransparent: 'rgba(25, 130, 86, .8)',
  skyblue: '#87CEFA'
};

export const fonts = {
  roboto: 'robotoCondensedRegular',
  robotoLight: 'robotoCondensedLight',
  robotoBold: 'robotoCondensedBold'
};

export const breakpoints = {
  small: '(max-width: 767px)',
  medium: '(min-width: 768px)',
  large: '(min-width: 1280px)'
};

export const spacings = {
  xs: '.5rem',
  s: '1rem',
  m: '1.5rem',
  l: '2rem',
  xl: '3rem'
};
