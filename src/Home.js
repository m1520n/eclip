/* globals window */
/* eslint-disable max-depth */

import React, { Component, Fragment } from 'react';
import { Helmet } from 'react-helmet';

import styled from 'styled-components';

import Slider from './components/slider';
import Separator from './components/separator';
import ColumnImages from './components/columns-images';
import Informations from './components/informations';
import Testimonials from './components/Testimonials';
import Gallery from './components/gallery';
import Contact from './components/contact';
import { debounce } from './utils';
import { colors } from './styles/variables';

const images = [
  { src: 'https://images.unsplash.com/photo-1533498286670-044582436fe5?ixlib=rb-0.3.5&s=cdd5d9c612b548a586fa519a6328f09e&auto=format&fit=crop&w=1200&q=60' },
  { src: 'https://images.unsplash.com/photo-1529924550447-6d6f091d31e1?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=d6d6683231115bf1e0cd1e7ffe387cff&auto=format&fit=crop&w=1200&q=60' },
  { src: 'https://images.unsplash.com/photo-1528543413928-5447744645a0?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a3d0df6a3936586edfbbb8684b32cdfb&auto=format&fit=crop&w=1200&q=60' },
  { src: 'https://images.unsplash.com/photo-1515259387710-51e175f9ec6d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=78de96a49822848b556494ca50b420df&auto=format&fit=crop&w=1200&q=60' },
  { src: 'https://images.unsplash.com/photo-1515405969538-5642ed9d0cc4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=510c435bb47ae2e87426f428fb002230&auto=format&fit=crop&w=1200&q=60' },
  { src: 'https://images.unsplash.com/photo-1514591404656-22ae99b3261c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=846f821b96e3fe06db1050e49f8f03f1&auto=format&fit=crop&w=1200&q=60' }
];

const SliderTestimonials = [
  {
    name: 'Michał / DevBrains.pl',
    description: '"Współpraca z firmą eClip była pełna profesjonalizmu i na pewno zlecę Panu Krzysztofowi kolejne realizacje marketingowe."'
  },
  {
    name: 'Patryk / Altsoft',
    description: `"Pan Krzysztof wykonał dla nas kompletny branding, który sprawił, że nasza marka stała się rozpoznawalna.`
  }
];

const informations = {
  heading: 'FOTOGRAFIA I FILM Z POWIETRZA',
  description: 'DRON - NOWE OBLICZE FOTOGRAFII',
  about: `Dzięki zastosowaniu nowych technologii firmy DJI możemy odkrywać dotychczas znane nam miejsca w
  zupełnie nowy sposób.
  Fotografia terenów inwestycyjnych,
  nowych budynków pozwoli na poprowadzenie rozmowy nie tylko o konkretnej nieruchomości, pozwoli pokazać okolicę,
  niedostrzegalne do tej pory plusy
  Kto nie chciałby mieć pamiątki z najważniejszych chwil naszego życia?
  Chrzciny, komunia święta, ślub... to tylko niektóre z chwil które warto uwiecznić.
  Połączenie fotografii z powietrza i naziemnej daje niesamowite efekty.`
};

const columns = [
  {
    heading: 'Fotografia z powietrza',
    description: 'Zobacz to czego inni nie widzą, dzięki wykorzystaniu drona możemy popatrzeć na wszystko z góry',
    image: 'https://images.unsplash.com/photo-1518995372969-424a232ca9ce?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=c5c6dff34b99791fad74fc5a52ebfd00&auto=format&fit=crop&w=768&q=70',
    icon: 'drone',
    iconColor: 'white',
    route: '/zdjecia'
  },
  {
    heading: 'Edycja wideo',
    description: 'Montaż filmów to najważniejsza część całego przedsięwzięcia.',
    image: 'https://images.unsplash.com/photo-1528109901743-12b16e05eedf?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=85289385ffd1eedef8ca3c3a012185c1&auto=format&fit=crop&w=768&q=70',
    icon: 'editing',
    iconColor: 'white',
    id: 'bmup2GwHeoE',
    route: '/filmy'
  },
  {
    heading: 'Reklama',
    description: 'Kreatywne reklamy, szyldy, ulotki i gadżety to coś co zostaje w pamięci.',
    image: 'https://images.unsplash.com/photo-1516131206008-dd041a9764fd?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5cdc76432883b62db78cb4b3f10e5d66&auto=format&fit=crop&w=768&q=70',
    icon: 'marketing',
    iconColor: 'white',
    route: '/reklama'
  }
];

const mainText = 'odmienny punkt widzenia';

const HomeWrapper = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  background-color: ${colors.snowWhite};
`;

class Home extends Component {
  constructor () {
    super();

    this.state = {
      sliderActive: 0,
      images,
      name: '',
      email: '',
      message: '',
      messageSent: false,
      isValidated: false,
      agreement: false,
      isLoading: true
    };

    this.debouncedOnScroll = debounce(this.onScroll, 50);
  }

  componentDidMount () {
    window.addEventListener('scroll', this.debouncedOnScroll);
    window.setTimeout(() => {
      this.setState({
        sliderActive: 1
      });
    }, 1000);
  }

  validateForm () {
    const formLength = this.formRef.length;

    if (!this.formRef.checkValidity()) {
      for (let i = 0; i < formLength; i++) {
        const elem = this.formRef[i];
        const errorLabel = elem.type === 'checkbox' ? elem.parentElement.nextSibling : elem.nextSibling;

        if (errorLabel && elem.nodeName.toLowerCase() !== 'button') {
          if (!elem.validity.valid) {
            errorLabel.textContent = elem.validationMessage;
          } else {
            errorLabel.textContent = '';
          }
        }
      }
      return false;
    } else {
      return true;
    }
  }

  onToggleMenu = () => {
    this.setState({
      menuExpanded: !this.state.menuExpanded
    });
  }

  onScroll = () => {
    this.setState({
      isTop: (window.scrollY < 50)
    });
  }

  onLoaded = () => {
    this.setState({
      isLoading: false
    });
  }

  onTextChange = (e) => {
    const { target } = e;
    const { name, value } = target;

    this.setState({
      [name]: value
    });
  }

  setContactFormRef = (el) => {
    this.formRef = el;
  }

  onMessageSend = (e) => {
    e.preventDefault();

    const { name, email, message } = this.state;

    const isValid = this.validateForm();
    this.setState({
      isValidated: isValid
    }, () => {
      if (!this.state.isValidated) return;

      try {
        window.fetch('https://mail.devbrains.pl/sendmail', {
          method: 'POST',
          cors: 'no-cors',
          headers: {
            "Content-Type": "application/json; charset=utf-8"
          },
          body: JSON.stringify({
            from: email,
            to: 'krzysiek@eclip.pl',
            text: message,
            html: message,
            subject: `wiadomosc z formularza eclip od ${name}`
          })
        });
      } catch (error) {
        console.log(error);
      }

      this.setState({
        messageSent: true
      }, () => setTimeout(() => {
        this.setState({
          name: '',
          email: '',
          message: '',
          agreement: false,
          messageSent: false
        });
      }, 2000));
    });
  }

  render () {
    const {
      sliderActive,
      images,
      email,
      name,
      message,
      messageSent,
      isValidated,
      agreement,
      videoExpanded,
      isLoading
    } = this.state;

    return (
      <Fragment>
        <Helmet>
          <title>eClip.pl - Odmienny punkt widzenia</title>
        </Helmet>
        <HomeWrapper>
          <Slider isLoading={isLoading} onLoaded={this.onLoaded} text={mainText} sliderActive={sliderActive} />
          <div id="offer"/>
          <Separator side="left" text="Nasza Oferta" icon="offer"/>
          <ColumnImages
            columns={columns}
            videoExpanded={videoExpanded}
            onClickVideo={this.onClickVideo}
          />

          <Separator side="left" text="Oni mi zaufali" icon="testimonial"/>
          <Testimonials SliderTestimonials={SliderTestimonials}/>

          <Separator side="right" text="Jak to powstaje" icon="design"/>
          <Informations
            about={informations.about}
            heading={informations.heading}
            description={informations.description}
          />

          <div id="gallery"/>
          <Separator side="right" text="Galeria" icon="camera"/>
          <Gallery
            images={images}
          />

          <div id="contact"/>
          <Separator side="right" text="Kontakt" icon="contact"/>
          <Contact
            name={name}
            email={email}
            message={message}
            agreement={agreement}
            messageSent={messageSent}
            isValidated={isValidated}
            setContactFormRef={this.setContactFormRef}
            onTextInput={this.onTextChange}
            onMessageSend={this.onMessageSend}
          />
        </HomeWrapper>
      </Fragment>
    );
  }
}

export default Home;
